import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  saveForm: FormGroup;
  saveData: any;


  constructor() { }

  ngOnInit() {
    this.saveForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl(''),
    });
  }
  save(saveForm) {
    this.saveData = JSON.stringify(saveForm.value);
    console.log(this.saveData);
    localStorage.setItem('myDataKey', this.saveData);
    console.log(saveForm);
  }

}
