import { TestBed } from '@angular/core/testing';

import { WholeServiceService } from './whole-service.service';

describe('WholeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WholeServiceService = TestBed.get(WholeServiceService);
    expect(service).toBeTruthy();
  });
});
