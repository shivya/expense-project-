import { Component, OnInit, Input } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
  budgets: any;
  saveData: any = {};
  moreOperationData: any = {};
  categories: any = [];

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) { }

  ngOnInit() {

  }
  // function to save total budget in local storage
  addBudget(setting) {
    this.saveData = JSON.parse(JSON.stringify(setting));
    console.log(this.saveData);
    localStorage.setItem('budgetData', this.saveData.budget);
  }

  // function to add category
  async addNote() {
    const alert = await this.alertCtrl.create({
      header: 'Add Categories',
      backdropDismiss: false,
      inputs: [
        {
          name: 'category',
          type: 'text',
          placeholder: 'Category'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (value) => {
            this.categories.push(value);
          }
        }
      ]
    });

    await alert.present();
  }
// function to delete category
  deleteCategory(data) {
    const index = this.categories.indexOf(data);
    if (index > -1) {
      this.categories.splice(index, 1);
    }
  }
}
