import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import {Chart} from 'chart.js';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  notes: any = [];
  userData: any;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
  }
  ngOnInit() {
  }
  ionViewWillEnter() {
  this.userData = localStorage.getItem('budgetData');
  console.log(this.userData);
  }
  // add function
  async addNote() {
    const alert = await this.alertCtrl.create({
      header: 'Add Note',
      backdropDismiss: false,
      inputs: [
        {
          name: 'category',
          type: 'text',
          placeholder: 'Category'
        },
        {
          name: 'itemName',
          type: 'text',
          placeholder: 'Item Name'
        },
        {
          name: 'amount',
          type: 'number',
          placeholder: 'Amount'
        },
        {
          name: 'date',
          type: 'date'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.notes.push(data);
          }
        }
      ]
    });

    await alert.present();
  }
  // edit function
  async editNote(note) {
    const alert = await this.alertCtrl.create({
      header: 'Edit Note',
      backdropDismiss: false,
      inputs: [
        {
          name: 'category',
          type: 'text',
          placeholder: 'Category'
        },
        {
          name: 'itemName',
          type: 'text',
          placeholder: 'Item Name'
        },
        {
          name: 'amount',
          type: 'number',
          placeholder: 'Amount'
        },
        {
          name: 'date',
          type: 'date'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            const index = this.notes.indexOf(note);
            if (index > -1) {
              this.notes[index] = data;
            }
          }
        }
      ]
    });

    await alert.present();
  }
  // delete function
  deleteNote(note) {
    const index = this.notes.indexOf(note);
    if (index > -1) {
      this.notes.splice(index, 1);
    }
  }
}
